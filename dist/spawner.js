"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _default = function _default(spawner) {
  if (spawner) {
    spawner.stdout.pipe(process.stdout);
    process.stdin.pipe(spawner.stdin);
  }
};

exports.default = _default;