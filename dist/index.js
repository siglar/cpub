#!/usr/bin/env node
"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _yargs = require("yargs");

var _store = _interopRequireDefault(require("./store"));

var _spawner = _interopRequireDefault(require("./spawner"));

var _authorize = _interopRequireDefault(require("./js/authorize"));

var _close = _interopRequireDefault(require("./js/close"));

var _open = _interopRequireDefault(require("./js/open"));

var _database = _interopRequireDefault(require("./js/database"));

var _initialize = _interopRequireDefault(require("./js/initialize"));

var _install = _interopRequireDefault(require("./js/install"));

var _publish = _interopRequireDefault(require("./js/publish"));

var _unpublish = _interopRequireDefault(require("./js/unpublish"));

var _ssh = _interopRequireDefault(require("./js/ssh"));

var _fix = _interopRequireDefault(require("./js/fix"));

var _info = _interopRequireDefault(require("./js/info"));

var _instances = _interopRequireDefault(require("./js/instances"));

var _projects = _interopRequireDefault(require("./js/projects"));

// Store repo path
var repoPath = (0, _store.default)('repoPath');
var hostPath = (0, _store.default)('hostPath');
var instance = (0, _store.default)('instance');
var project = (0, _store.default)('project');
var port = (0, _store.default)('port');
var offset = (0, _store.default)('offset');
var hostRunnableFile = (0, _store.default)('hostRunnableFile');

if (_yargs.argv.ins) {
  console.log(instance);
}

if (_yargs.argv.proj) {
  console.log(project);
}

if (_yargs.argv.auth && project) {
  (0, _spawner.default)((0, _authorize.default)(project));
}

if (_yargs.argv.close && instance && port) {
  (0, _spawner.default)((0, _close.default)(instance, port));
}

if (_yargs.argv.open && instance && project && port) {
  (0, _spawner.default)((0, _open.default)(instance, project, port));
}

if (_yargs.argv.database && instance && offset) {
  (0, _spawner.default)((0, _database.default)(instance, offset));
}

if (_yargs.argv.init && instance && repoPath && hostPath) {
  (0, _spawner.default)((0, _initialize.default)(instance, repoPath, hostPath));
}

if (_yargs.argv.install && instance) {
  (0, _spawner.default)((0, _install.default)(instance));
}

if (_yargs.argv.publish && instance && hostPath && hostRunnableFile) {
  (0, _spawner.default)((0, _publish.default)(instance, hostPath, hostRunnableFile));
}

if (_yargs.argv.unpublish && instance && hostPath && hostRunnableFile) {
  (0, _spawner.default)((0, _unpublish.default)(instance, hostPath, hostRunnableFile));
}

if (_yargs.argv.info) {
  (0, _spawner.default)((0, _info.default)());
}

if (_yargs.argv.projects) {
  (0, _spawner.default)((0, _projects.default)());
}

if (_yargs.argv.instances) {
  (0, _spawner.default)((0, _instances.default)());
}

if (_yargs.argv.fix && instance) {
  (0, _spawner.default)((0, _fix.default)(instance));
}

if (_yargs.argv.ssh && instance) {
  (0, _spawner.default)((0, _ssh.default)(instance));
}