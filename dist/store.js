"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _yargs = require("yargs");

var _nodeLocalstorage = require("node-localstorage");

var localStorage = new _nodeLocalstorage.LocalStorage('./cpub');

var _default = function _default(key) {
  var value = _yargs.argv[key];

  if (typeof value === 'boolean' && value || typeof value === 'undefined') {
    value = localStorage.getItem(key);
  } else if (value) {
    localStorage.setItem(key, value);
  }

  return value;
};

exports.default = _default;