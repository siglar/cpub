"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _child_process = require("child_process");

var _path = _interopRequireDefault(require("path"));

var _default = function _default(instance) {
  var spawner = (0, _child_process.spawn)('../../bin/fix.sh', [instance], {
    cwd: _path.default.resolve(__dirname)
  });
  process.stdin.pipe(spawner.stdin);
  return spawner;
};

exports.default = _default;