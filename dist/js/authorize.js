"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _shelljs = _interopRequireDefault(require("shelljs"));

var _default = function _default(project) {
  _shelljs.default.exec('gcloud auth login');

  _shelljs.default.exec('gcloud config set project ' + project);

  _shelljs.default.exec('gcloud compute config-ssh');

  _shelljs.default.exec('echo Done!');

  return false;
};

exports.default = _default;