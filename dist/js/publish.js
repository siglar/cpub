"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _child_process = require("child_process");

var _path = _interopRequireDefault(require("path"));

var _default = function _default(instance, hostPath, hostRunnableFile) {
  var cmd = '';
  cmd += 'echo "Publish started, fetching updates..."' + '; ';
  cmd += 'cd ' + hostPath + '; ';
  cmd += 'ls' + '; ';
  cmd += 'git fetch --all' + '; ';
  cmd += 'git reset --hard origin/master' + '; ';
  cmd += 'echo "Installing deps"' + '; ';
  cmd += 'npm i' + '; ';
  cmd += 'echo "Restarting db"' + '; ';
  cmd += 'sudo /etc/init.d/rethinkdb restart' + '; ';
  cmd += 'echo Restarting service' + '; ';
  cmd += 'sudo forever stop ' + hostRunnableFile + '; ';
  cmd += 'sudo forever start --minUptime 20000 --spinSleepTime 20000 ' + hostRunnableFile + '; ';
  cmd += 'echo "Your update was successfully published!"' + '; ';
  var spawner = (0, _child_process.spawn)("ssh ".concat(instance, " -tt ") + cmd, {
    cwd: _path.default.resolve(__dirname),
    shell: true
  });
  return spawner;
};

exports.default = _default;