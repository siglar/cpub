"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _child_process = require("child_process");

var _path = _interopRequireDefault(require("path"));

var _default = function _default(instance, project, port) {
  var spawner = (0, _child_process.exec)("ssh -t ".concat(instance, " gcloud compute firewall-rules create \"allowport-").concat(port, "\" --allow \"tcp:").concat(port, "\" --source-tags=\"").concat(project, "\" --source-ranges=0.0.0.0/0"), {
    cwd: _path.default.resolve(__dirname)
  });
  return spawner;
};

exports.default = _default;