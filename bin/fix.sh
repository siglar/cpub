#! /bin/bash

instance="$1"

blue=`tput setaf 4`
green=`tput setaf 2`
reset=`tput sgr0`

# Fix apt-get
ssh $instance -t << EOF

sudo rm /var/cache/debconf/*.dat

sudo rm /var/lib/apt/lists/lock
sudo rm /var/cache/apt/archives/lock
sudo rm /var/lib/dpkg/lock
sudo dpkg --configure -a

sudo apt --fix-broken install
sudo apt autoremove -y

sudo apt-get purge grub-pc grub-common -y
sudo rm -r /etc/grub.d/

sudo apt-get install grub-pc grub-common -y
sudo grub-install /dev/sda
sudo update-grub


EOF

echo -e "\n${green}Done.${reset}\n"