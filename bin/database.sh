#! /bin/bash

instance="$1"
offset=$2

magenta=`tput setaf 5`
blue=`tput setaf 4`
green=`tput setaf 2`
reset=`tput sgr0`

# UPDATE DATABASE
ssh $instance << EOF

echo -e "\n${blue}Updating database configuration${reset}\n"

# Copy example file (Not neccesarry, but nice to know where it is)
sudo cp /etc/rethinkdb/default.conf.sample /etc/rethinkdb/instances.d/instance1.conf

# Clear it
sudo truncate -s 0 /etc/rethinkdb/instances.d/instance1.conf

# Write to it
echo "
server-name=Kismet
server-tag=default
port-offset=${offset}
bind=all # warning, this makes it public
daemon
" | sudo tee -a /etc/rethinkdb/instances.d/instance1.conf

echo -e "\n${magenta}Restarting server${reset}\n"
sudo /etc/init.d/rethinkdb restart

EOF

echo -e "\n${green}Database available on port ${magenta}$((8080 + $offset)) ${green} Client port: ${magenta}$((28015 + $offset))${reset}\n"