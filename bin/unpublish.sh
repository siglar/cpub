#! /bin/bash

# Publish updates to host
instance="$1"
path="$2"
main="$3"

magenta=`tput setaf 5`
blue=`tput setaf 4`
green=`tput setaf 2`
reset=`tput sgr0`

ssh $instance << EOF

echo -e "\n${blue}Stopping service${reset}\n"
cd $path
sudo forever stop $main

echo -e "\n${blue}Stopping database${reset}\n"
sudo /etc/init.d/rethinkdb stop

echo -e "\n${green}All services stopped!${reset}\n"

EOF