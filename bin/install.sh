#! /bin/bash

# Install all dependencies on host server
instance="$1"

magenta=`tput setaf 5`
blue=`tput setaf 4`
green=`tput setaf 2`
reset=`tput sgr0`

ssh $instance << EOF

echo -e "\n${blue}Connected to host, trying to update dependencies.${reset}\n"

# Upgrade to latest packages
sudo apt-get update
sudo apt-get upgrade

# Install nodejs
echo -e "\n${magenta}Installing nodejs (javascript runtime).${reset}\n"
curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
sudo apt-get install nodejs -y

# Install npm
echo -e "\n${magenta}Installing npm (javascript package manager).${reset}\n"
sudo apt-get install npm -y

# Install git
echo -e "\n${magenta}Installing git (version controll).${reset}\n"
sudo apt-get install git -y

# Install database (RethinkDB)
echo -e "\n${magenta}Installing RethinkDB (database).${reset}\n"

## Add to list of repositories
echo "deb http://download.rethinkdb.com/apt \`lsb_release -cs\` main" | sudo tee /etc/apt/sources.list.d/rethinkdb.list
wget -qO- https://download.rethinkdb.com/apt/pubkey.gpg | sudo apt-key add -

## Update package manager
sudo apt-get update

## Install dependency for debian
wget http://mirrors.kernel.org/ubuntu/pool/main/p/protobuf/libprotobuf9v5_2.6.1-1.3_amd64.deb
sudo dpkg -i libprotobuf9v5_2.6.1-1.3_amd64.deb

sudo apt-get install rethinkdb

# Install global node dependencies
echo -e "\n${magenta}Installing forever (service to keep server running).${reset}\n"
sudo npm install forever -g

EOF

echo -e "\n${green}Installations comlete! you may now run ${green}pub --init to initialize project on host.${reset}\n"