#! /bin/bash

# Install all dependencies on host server
instance="$1"
repo="$2"
project="$3"
execdir="$4"

magenta=`tput setaf 5`
blue=`tput setaf 4`
green=`tput setaf 2`
reset=`tput sgr0`

# Login to gcloud on host
echo -e "\n${blue}Initialization started, trying to login.${reset}\n"
ssh $instance -t 'gcloud auth login'

# Setup ssh access on gcloud host
echo -e "\n${blue}Setting up ssh configuration on host.${reset}\n"
ssh $instance 'gcloud compute config-ssh'

ssh $instance << EOF

# Create repo
echo -e "\n${magenta}Creating repository${reset}\n"
mkdir $repo
sudo chmod 777 -R $repo
cd $repo
git init --bare

echo -e "\n${magenta}Creating host directory${reset}\n"
# Clone repo in host folder
mkdir $project
sudo chmod 777 -R $project
cd $project
echo "git clone $instance:$repo ."
git clone $instance:$repo .

EOF

cd $execdir
pwd

# Add host repository as local remote & push files
echo -e "\n${magenta}Adding repository to local computer${reset}\n"
echo "git remote add live $instance:$repo"
git init
git remote add live $instance:$repo

echo -e "\n${magenta}Creating & pushing initial commit${reset}\n"
git add .
git commit -m "Init"
git push live master

# Publish current files to the host serving directory (host directory)
echo -e "\n${magenta}Publishing to host directory${reset}\n"
pub --publish

echo "${green}Initialization complete. You can now update host server by running ${magenta}pub --publish --hostRunnableFile dist/your_serve_file.js${reset}"
echo "${green}Start database by running ${magenta}pub --database${reset}"