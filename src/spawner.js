export default (spawner) => {
  if (spawner) {
    spawner.stdout.pipe(process.stdout)
    process.stdin.pipe(spawner.stdin)
  }
}
