import { spawn } from 'child_process'
import path from 'path'

export default (instance, repoPath, hostPath) => {
  let cmd = ''
  cmd += 'echo "Initialization started, trying to login."' + '&&'
  cmd += `ssh ${instance} -tt `
  cmd += 'gcloud auth login' + '; '
  cmd += 'echo "Setting up ssh configuration on host"' + '; '
  cmd += 'gcloud compute config-ssh' + '; '
  cmd += 'echo "Creating repository"' + '; '
  cmd += 'sudo mkdir -p ' + repoPath + '; '
  cmd += 'sudo chmod 777 -R ' + repoPath + '; '
  cmd += 'cd ' + repoPath + '; '
  cmd += 'git init --bare' + '; '
  cmd += 'echo "Creating host directory"' + '; '
  cmd += 'sudo mkdir -p ' + hostPath + '; '
  cmd += 'sudo chmod 777 -R ' + hostPath + '; '
  cmd += 'cd ' + hostPath + '; '
  cmd += `echo "git clone ${instance}:${repoPath}"` + '; '
  cmd += `git clone ` + instance + ':' + repoPath + '; '
  cmd += '\\n'
  cmd += `cd` + process.cwd() + '; '
  cmd += 'pwd' + '; '
  cmd += 'git init' + '; '
  cmd += 'echo "Adding repository to local computer"' + ';'
  cmd += `echo "git remote add live ${instance}:${repoPath}"` + '; '
  cmd += 'git remote add live ' + instance + ':' + repoPath + '; '
  cmd += 'echo "Creating & pushing initial commit"' + '; '
  cmd += 'git add .' + '; '
  cmd += 'git commit -m "Init"' + '; '
  cmd += 'git push live master' + '; '
  cmd += 'echo "Publishing to host directory"' + '; '
  cmd += 'pub --publish' + '; '
  cmd += 'echo "Initialization complete. You can now update host server by running pub --publish --hostRunnableFile dist/your_serve_file.js"' + '; '
  cmd += 'echo "Start database by running pub --database"' + '; '

  const spawner = spawn(cmd, {
    cwd: path.resolve(__dirname),
    shell: true
  })
  return spawner
}
