import { exec } from 'child_process'
import path from 'path'

export default (instance, port) => {
  const spawner = exec('ssh -t ' + instance + ' gcloud compute firewall-rules delete "allowport-' + port + '"', {
    cwd: path.resolve(__dirname)
  })
  return spawner
}
