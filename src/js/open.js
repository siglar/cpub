import { exec } from 'child_process'
import path from 'path'

export default (instance, project, port) => {
  const spawner = exec(`ssh -t ${instance} gcloud compute firewall-rules create "allowport-${port}" --allow "tcp:${port}" --source-tags="${project}" --source-ranges=0.0.0.0/0`, {
    cwd: path.resolve(__dirname)
  })
  return spawner
}
