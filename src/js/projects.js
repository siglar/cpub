import { exec } from 'child_process'
import path from 'path'

export default () => {
  const spawner = exec('gcloud projects list', {
    cwd: path.resolve(__dirname)
  })
  process.stdin.pipe(spawner.stdin)
  return spawner
}
