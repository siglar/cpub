import shell from 'shelljs'

export default (project) => {
  shell.exec('gcloud auth login')
  shell.exec('gcloud config set project ' + project)
  shell.exec('gcloud compute config-ssh')
  shell.exec('echo Done!')
  return false
}
