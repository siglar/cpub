import { spawn } from 'child_process'
import path from 'path'

export default (instance, offset) => {
  const spawner = spawn('../../bin/database.sh', [instance, offset], {
    cwd: path.resolve(__dirname)
  })
  process.stdin.pipe(spawner.stdin)
  return spawner
}
