import { spawn } from 'child_process'
import path from 'path'

export default (instance) => {
  const spawner = spawn('../../bin/install.sh', [instance], {
    cwd: path.resolve(__dirname)
  })
  process.stdin.pipe(spawner.stdin)
  return spawner
}
