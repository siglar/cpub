import { exec } from 'child_process'
import path from 'path'

export default () => {
  const spawner = exec(`gcloud compute instances list`, {
    cwd: path.resolve(__dirname)
  })
  return spawner
}
