import { spawn } from 'child_process'
import path from 'path'

export default (instance, hostPath, hostRunnableFile) => {
  const spawner = spawn('../../bin/unpublish.sh', [instance, hostPath, hostRunnableFile], {
    cwd: path.resolve(__dirname)
  })
  process.stdin.pipe(spawner.stdin)
  return spawner
}
