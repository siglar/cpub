import { spawn } from 'child_process'
import path from 'path'

export default (instance) => {
  const spawner = spawn(`ssh`, ['-tt', instance], {
    cwd: path.resolve(__dirname)
  })
  return spawner
}
