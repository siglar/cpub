import { spawn } from 'child_process'
import path from 'path'

export default (instance, hostPath, hostRunnableFile) => {
  let cmd = ''
  cmd += 'echo "Publish started, fetching updates..."' + '; '
  cmd += 'cd ' + hostPath + '; '
  cmd += 'ls' + '; '
  cmd += 'git fetch --all' + '; '
  cmd += 'git reset --hard origin/master' + '; '
  cmd += 'echo "Installing deps"' + '; '
  cmd += 'npm i' + '; '
  cmd += 'echo "Restarting db"' + '; '
  cmd += 'sudo /etc/init.d/rethinkdb restart' + '; '
  cmd += 'echo Restarting service' + '; '
  cmd += 'sudo forever stop ' + hostRunnableFile + '; '
  cmd += 'sudo forever start --minUptime 20000 --spinSleepTime 20000 ' + hostRunnableFile + '; '
  cmd += 'echo "Your update was successfully published!"' + '; '
  const spawner = spawn(`ssh ${instance} -tt ` + cmd, {
    cwd: path.resolve(__dirname),
    shell: true
  })
  return spawner
}
