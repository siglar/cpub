import { argv } from 'yargs'
import { LocalStorage } from 'node-localstorage'

const localStorage = new LocalStorage('./cpub')

export default (key) => {
  let value = argv[key]
  if ((typeof value === 'boolean' && value) || typeof value === 'undefined') {
    value = localStorage.getItem(key)
  } else if (value) {
    localStorage.setItem(key, value)
  }
  return value
}
