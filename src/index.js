#!/usr/bin/env node

import { argv } from 'yargs'
import store from './store'
import spawner from './spawner'
import authorize from './js/authorize'
import close from './js/close'
import open from './js/open'
import database from './js/database'
import initialize from './js/initialize'
import install from './js/install'
import publish from './js/publish'
import unpublish from './js/unpublish'
import ssh from './js/ssh'
import fix from './js/fix'
import info from './js/info'
import instances from './js/instances'
import projects from './js/projects'

// Store repo path
const repoPath = store('repoPath')
const hostPath = store('hostPath')
const instance = store('instance')
const project = store('project')
const port = store('port')
const offset = store('offset')
const hostRunnableFile = store('hostRunnableFile')

if (argv.ins) {
  console.log(instance)
}

if (argv.proj) {
  console.log(project)
}

if (argv.auth && project) {
  spawner(authorize(
    project
  ))
}
if (argv.close && instance && port) {
  spawner(close(
    instance,
    port
  ))
}
if (argv.open && instance && project && port) {
  spawner(open(
    instance,
    project,
    port
  ))
}
if (argv.database && instance && offset) {
  spawner(database(
    instance,
    offset
  ))
}
if (argv.init && instance && repoPath && hostPath) {
  spawner(initialize(
    instance,
    repoPath,
    hostPath
  ))
}
if (argv.install && instance) {
  spawner(install(
    instance
  ))
}
if (argv.publish && instance && hostPath && hostRunnableFile) {
  spawner(publish(
    instance,
    hostPath,
    hostRunnableFile
  ))
}
if (argv.unpublish && instance && hostPath && hostRunnableFile) {
  spawner(unpublish(
    instance,
    hostPath,
    hostRunnableFile
  ))
}
if (argv.info) {
  spawner(info(
  ))
}
if (argv.projects) {
  spawner(projects(
  ))
}
if (argv.instances) {
  spawner(instances(
  ))
}
if (argv.fix && instance) {
  spawner(fix(
    instance
  ))
}
if (argv.ssh && instance) {
  spawner(ssh(
    instance
  ))
}
