module.exports = {
    root: true,
    parserOptions: {
        ecmaVersion: 6,
        sourceType: 'module'
    },
    extends: 'airbnb-base',
    env: {
        browser: true,
        node: true
    },
    rules: {
        'quotes': 0,
        'no-useless-concat': 0,
        'linebreak-style': 0,
        'no-console': 0,
        'semi': [2, 'never'],
        'prefer-template': 0,
        'operator-linebreak': [2, 'after', { 'overrides': { '?': 'before' } }],
        'comma-dangle': ['error', {
            arrays: 'never',
            objects: 'never',
            imports: 'never',
            exports: 'never',
            functions: 'ignore'
        }],
        'prefer-destructuring': ['error', {
            array: false,
            object: false
        }, {
            'enforceForRenamedProperties': false
        }]
    }
}
