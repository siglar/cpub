# Cloud Publisher

## About

CLI to simplify / automate publishment of Google Compute Engine projects.

## Purpose

Automated setup for running nodejs servers with simple distribution.

### **Requirements**

- NodeJS
- Git
- Debian 9 stretch VM (Virtual Machine) on Google Compute Engine: https://console.cloud.google.com/compute/instances
- Install gcloud SDK: https://cloud.google.com/sdk/install
- Mac or Linux. This is currently not supported for Windows *(Unless you can run **.sh** files)*.

### **Installation**

```bash
npm i -g @siglar/cpub
```

### **Commands**

```bash
# Use to repair apt-get if broken
# WARNING! Use carefully. Read more here:
# https://askubuntu.com/questions/15433/unable-to-lock-the-administration-directory-var-lib-dpkg-is-another-process
pub --fix

# Info about current project
pub --info

# List projects
pub --projects

# Set current project
pub --project project

# Show current project
pub --proj

# Connect / authorize to work with current project
pub --auth

# List instances
pub --instances

# Show current instance
pub --ins

# Set current instance
pub --instance NAME.ZONE.PROJECT_ID
# Get NAME and ZONE from [ pub --instances ]
# Get PROJECT_ID from [ pub --projects ]

# Install dependencies on server
pub --install

# Set repoPath (Required to init)
pub --repoPath /path/to/new/repo
# Example: /home/webservice/repo

# Set hostPath (Required to init & publish / unpublish)
pub --hostPath /path/to/server/dir
# Example: /home/webservice/server

# Initialize new repository
# After this, push to server by: git push live master
pub --init

# Set host file (Required to publish / unpublish)
pub --hostRunnableFile dist/my_server.js

# Publish / distribute changes
pub --publish

# Unpublish / shut down server
# Does also stop database server
pub --unpublish

# Set port offset for database (Required for database)
# Client port = 28015 + offset & dashboard on port 8080 + offset
pub --offset 0

# Start / restart database
pub --database

# Set port number (required to open & close)
pub --port 8080

# Open Port
pub --open

# Close Port
pub --close

# ssh in to server for manual changes & fixes. (Currently shows command.)
pub --ssh

```

### Notice

All arguments can be passed simultaneously.

#### Example

```bash
pub --open --port 8080
pub --instance NAME.ZONE.PROJECT_ID --install
pub --ssh --instance NAME.ZONE.PROJECT_ID
```

The latest configured values which are set will be used.

___

## Grouped by Functionality

### Methods

```bash
pub --auth
pub --install
pub --init
pub --publish
pub --unpublish
pub --database
pub --open
pub --close
pub --ssh
pub --fix
```

### Setters

```bash
pub --project project
pub --instance NAME.ZONE.PROJECT_ID
pub --repoPath /path/to/new/repo
pub --hostPath /path/to/server/dir
pub --hostRunnableFile dist/my_server.js
pub --offset 0
pub --port 8080
```

### Getters

```bash
pub --info
pub --projects
pub --proj
pub --instances
pub --ins
```